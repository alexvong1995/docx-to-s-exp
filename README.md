Readme for docx-to-s-exp
========================

Run-time dependencies
---------------------
* guile-2.0
* guile-json
* pandoc

Instruction for Debian users
----------------------------
The easiest way to try this software is to install Debian Testing,
and run the following commands:

    $ su -
    # apt-get update
    # apt-get upgrade
    # apt-get install guile guile-json pandoc

Usage
-----
    Usage: ./docx-to-s-exp [OPTION] [FILE] ...
    Convert docx FILE(s) to s-expressions.

      -h, --help         Display this help

License
-------
GPL version 3 or later
